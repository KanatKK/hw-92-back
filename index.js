const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config");
const users = require("./app/users");
const expressWs = require("express-ws");
const {nanoid} = require("nanoid");
expressWs(app);

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const messages = [];
const activeConnections = {};
app.ws("/chat", (ws,req) => {
    const id = nanoid();
    activeConnections[id] = ws;

    let displayName;
    console.log(activeConnections);
    ws.on("message", msg => {
        const decodedMessage = JSON.parse(msg);

        switch (decodedMessage.type) {
            case "CONNECT_USER":
                displayName = decodedMessage.user.displayName
                break;
            case "GET_ALL_MESSAGES":
                ws.send(JSON.stringify({type: "ALL_MESSAGES", messages}));
                break;
            case "CREATE_MESSAGE":
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    messages.push({
                        displayName,
                        text: decodedMessage.text
                    });
                    conn.send(JSON.stringify({
                        type: "NEW_MESSAGE",
                        message: {
                            displayName,
                            text: decodedMessage.text
                        }
                    }));
                });
                break;
            default:
                console.log("Unknown message type:", decodedMessage.type);
        }
    });

    ws.on("close", msg => {
        delete activeConnections[id];
        console.log("disconnected!");
    });
})

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

    app.use("/users", users);

    console.log("Connected to mongo DB");

    app.listen(8000, () => {
        console.log("Server started at http://localhost:8000");
    });
}

run().catch(console.log);