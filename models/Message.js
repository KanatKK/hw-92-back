const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    message: {
        type: String,
        required: true,
    },
    author: {
        type: Object,
        required: true
    }
});

MessageSchema.methods.generateAuthor = function (author) {
    this.author = author;
};

const Message = mongoose.model("Message", MessageSchema);

module.exports = Message;